# My python API

### This project is to demonstrate various use cases of a api, pipeline, deployment etc. Feel free to fork it for your benefits.
* Reference: https://dev.to/codemaker2015/build-and-deploy-flask-rest-api-on-docker-25mf
### Steps to create a Flask Python API:
* python3 -m venv venv ==> create the venv env
* source venv/bin/activate ==> to activate the venv
* any pip command will not be installed in this new env
* to deactivate the venv
* install flask: pip install flask
* create the app.py flask API
* ```export FLASK_APP=app```
* ```export FLASK_ENV=development ```

#### how to run: 
* python app.py
* How to capture the requirement in txt file ==> ```pip freeze > requirements.txt```
* Add Docker file
* run docker build and tag and run and push
docker build -t my-flask-api:v1 .
docker images | grep "flask"
docker run -p5001:5000 my-flask-api 
docker tag my-flask-api:v1  333743/my-flask-api:v1
docker push 333743/my-flask-api:v1

#### testing:
* to run python test:  python -m unittest test_app.py
* coverage run --omit="test_*" -m unittest test_app.py
* coverage html 
* this will generate an html report under: htmlcov
* Image can be can be downloaded from here: https://hub.docker.com/333743/my-flask-api


### Helm Commands:
* ```helm create chart```
* ```helm install my-api-chart ./my-api-chart --dry-run --debug --set service.type=NodePort```
* ```helm upgrade my-api-chart ./my-api-chart --set service.type=NodePort```
* ```helm registry login registry-1.docker.io``` I am using Docker hub to store the Helm chart. So for login enter docker hub user name and password
* ```helm list```
* ```helm uninstall chart```
* ```helm push my-flask-api-chart-0.1.0.tgz oci://registry-1.docker.io/333743```
* Note: If it finds the name in the repo then it wil add the helm chart there other wise it will create the new
* Use this command to run helm/kubectl from docker : ```docker run -it -v ~/.kube:/root/.kube dtzar/helm-kubectl```. The -v will mount your kube config file to the docker container.
* To use this image in the gitlab pipeline, check the ```.gitlab-ci.yml``` file
